﻿namespace MongoDbDemoBlog
{
    using System.Configuration;

    using MongoDB.Bson.Serialization.Conventions;
    using MongoDB.Driver;

    /// <summary>
    /// The mongo database configuration.
    /// </summary>
    public class MongoDbConfiguration
    {
        /// <summary>
        /// Configures the mongo database
        /// </summary>
        /// <returns>
        /// The <see cref="MongoDatabase"/>.
        /// </returns>
        public static MongoDatabase ConfigureMongo()
        {
            var server = new MongoClient(ConfigurationManager.ConnectionStrings["localMongo"].ConnectionString).GetServer();
            var mongoDb = server.GetDatabase("blog");

            var myConventions = new ConventionPack
                                    {
                                        new CamelCaseElementNameConvention(),
                                        new IgnoreExtraElementsConvention(true)
                                    };

            ConventionRegistry.Register(
               "Conventions",
               myConventions,
               t => t.IsClass);

            return mongoDb;
        }

        /// <summary>
        /// Clears the database.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        public static void ClearDatabase(MongoDatabase database)
        {
            database.Drop();
        }
    }
}