﻿namespace MongoDbDemoBlog.Elements
{
    using MongoDB.Bson;

    public class Comment
    {
        public string Text { get; set; }

        public string Email { get; set; }
    }
}