﻿namespace MongoDbDemoBlog.Elements
{
    using System;
    using System.Collections.Generic;

    using MongoDB.Bson;

    public class Post
    {
        public Post()
        {
            Time = DateTime.UtcNow;
            Tags = new List<string>();
            Comments = new List<Comment>();
        }

        public ObjectId Id { get; set; }

        public DateTime Time { get; set; }

        public ObjectId By { get; set; }

        public string Header { get; set; }

        public string Body { get; set; }

        public List<string> Tags { get; set; }

        public List<Comment> Comments { get; set; }
    }
}