﻿namespace MongoDbDemoBlog.Elements
{
    using MongoDB.Bson;

    public class Blogger
    {
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}