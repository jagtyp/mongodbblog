﻿using System;
using System.Linq;

namespace MongoDbDemoBlog
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using MongoDB.Driver.Builders;

    using MongoDbDemoBlog.Elements;

    class Program
    {
        private static MongoDatabase mongodb;

        static void Main(string[] args)
        {
            mongodb = MongoDbConfiguration.ConfigureMongo();

            var selectedBlog = ObjectId.Empty;

            while (true)
            {
                Console.WriteLine("Choose what you want to do:");
                Console.WriteLine("1. Choose blog");
                Console.WriteLine("2. Add blogger");
                Console.Write("Option: ");
                var chosen = Console.ReadLine();

                if (chosen == "1")
                {
                    selectedBlog = ChooseBlog();
                    break;
                }
                else if (chosen == "2")
                {
                    AddBlogger();
                }
                else
                {
                    Console.WriteLine("Choose a valid option!");
                }
            }

            while (true)
            {
                Console.WriteLine("Choose what you want to do:");
                Console.WriteLine("1. View posts");
                Console.WriteLine("2. Add post");
                Console.WriteLine("q. Quit");
                Console.Write("Option: ");
                var chosen = Console.ReadLine();

                if (chosen == null)
                {
                    continue;
                }

                if (chosen == "1")
                {
                    ViewPosts(selectedBlog);
                }
                else if (chosen == "2")
                {
                    AddPost(selectedBlog);
                }
                else if (chosen.ToLowerInvariant() == "q")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Choose a valid option!");
                }
            }


            Console.WriteLine("Your done in here!");
            Console.ReadLine();
        }

        private static void AddBlogger()
        {
            var blogger = new Blogger();
            Console.WriteLine("Add a blogger:");

            Console.Write("Name: ");
            blogger.Name = Console.ReadLine();

            Console.Write("Email: ");
            blogger.Email = Console.ReadLine();

            var bloggerCollection = GetBloggerCollection();
            bloggerCollection.Save(blogger);
        }

        static ObjectId ChooseBlog()
        {
            var bloggerCollection = GetBloggerCollection();

            var bloggers = bloggerCollection.FindAll()
                .SetSortOrder(SortBy<Blogger>.Ascending(x => x.Name));

            var counter = 0;

            foreach (var blogger in bloggers)
            {
                counter++;
                Console.WriteLine("{0}. {1} ({2})", counter, blogger.Name, blogger.Email);
            }

            while (true)
            {
                Console.Write("Choose blog: ");
                var chosen = Console.ReadLine();

                int index;
                if (int.TryParse(chosen, out index))
                {
                    var blogger = bloggers.ElementAtOrDefault(index - 1);

                    if (blogger != null)
                    {
                        return blogger.Id;
                    }
                }

                Console.WriteLine("Please enter a valid number!");
            }
        }

        private static void ViewPosts(ObjectId selectedBlog)
        {
            var postsCollection = GetPostsCollection();

            var query = Query<Post>.EQ(x => x.By, selectedBlog);

            var posts = postsCollection.Find(query)
                .SetSortOrder(SortBy<Post>.Descending(x => x.Time));

            var counter = 0;

            foreach (var post in posts)
            {
                counter++;
                Console.WriteLine("{0}. {1} - {2} - {3} comments", counter, post.Header, post.Time, post.Comments.Count());
            }

            while (true)
            {
                Console.Write("Choose blog (q to go back):  ");
                var chosen = Console.ReadLine();

                if (chosen == null)
                {
                    continue;
                }

                if (chosen.ToLower().Equals("q"))
                {
                    return;
                }

                int index;
                if (int.TryParse(chosen, out index))
                {
                    var post = posts.ElementAtOrDefault(index - 1);

                    if (post != null)
                    {
                        ViewPost(post);
                        return;
                    }
                }

                Console.WriteLine("Please enter a valid number!");
            }
        }

        private static void ViewPost(Post post)
        {
            Console.WriteLine("{0} - {1}", post.Header, post.Time);
            Console.WriteLine();
            Console.WriteLine("{0}", post.Body);
            Console.WriteLine();

            if (post.Tags.Any())
            {
                Console.WriteLine("{0}", string.Join(", ", post.Tags));
            }

            foreach (var comment in post.Comments)
            {
                Console.WriteLine("--------- {0} ---------", comment.Email);
                Console.WriteLine();
                Console.WriteLine(comment.Text);
                Console.WriteLine();
            }

            Console.WriteLine("--------------------------");

            while (true)
            {
                Console.WriteLine("Enter a comment or press enter to go back.");
                var commentText = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(commentText))
                {
                    return;
                }

                Console.Write("Enter your email: ");
                var email = Console.ReadLine();

                var comment = new Comment { Text = commentText, Email = email };

                var postsCollection = GetPostsCollection();
                var query = Query<Post>.EQ(x => x.Id, post.Id);
                var update = Update<Post>.Push(x => x.Comments, comment);

                postsCollection.Update(query, update);
            }
        }

        private static void AddPost(ObjectId selectedBlog)
        {
            var blogPost = new Post
                {
                    By = selectedBlog
                };

            Console.WriteLine("Add a blog post");
            Console.WriteLine();

            Console.Write("Header: ");
            blogPost.Header = Console.ReadLine();

            Console.Write("Body: ");
            blogPost.Body = Console.ReadLine();

            Console.WriteLine("Add tags. Enter q when you'r done: ");

            while (true)
            {
                Console.Write("Tag: ");
                var tag = Console.ReadLine();

                if (tag == null)
                {
                    continue;
                }

                if (tag.ToLowerInvariant() == "q")
                {
                    break;
                }

                blogPost.Tags.Add(tag.Trim());
            }

            var postsCollection = GetPostsCollection();
            postsCollection.Save(blogPost);
        }

        private static MongoCollection<Blogger> GetBloggerCollection()
        {
            var bloggerCollection = mongodb.GetCollection<Blogger>("bloggers");
            return bloggerCollection;
        }

        private static MongoCollection<Post> GetPostsCollection()
        {
            var postsCollection = mongodb.GetCollection<Post>("posts");
            return postsCollection;
        }
    }
}
